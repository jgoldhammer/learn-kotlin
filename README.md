# Kotlin Workshop - JAX 2020

von René Preißel und Bjørn Stachmann

## Workshop - Konzept

* Kotlin für Java-Entwickler praxisnah vorstellen 
* "Hands on"  
    * Schneller Einstieg in praktische Übungen
    * Durchgängiges Beispiel auf Basis einer Webanwendung
    * Bei Null starten und jede Codezeile selber schreiben 
    * Viel Raum zum experimentieren
* Nur die wichtigsten Features vorstellen
* Fokus auf Unterschiede zu Java
* Am Ende ein Ausblick auf weitere coole Features von Kotlin

## Agenda

### Vorbereitung vor dem Workshop

Es ist sehr empfehlenswert, 
eine Kotlin-Entwicklungsumgebung schon vor dem Workshop zu installieren:

[Installationsanleitung](teil0-installation/index.md)

### Intro und Vorstellung

### Phase I: *Mehr als nur "Hallo Welt"*

Das Gerüst für eine Webanwendung wird aufgebaut. 
Sie erlernen dabei: Grundlegende Syntax, 
einfache Funktionen deklarieren und aufrufen, 
einfache Logiken und Fallunterscheidungen, 
das Starten eines Web-Servers 
und das Bearbeiten von http-Requests mit Hilfe von Ktor.

[Einstieg in Kotlin und Ktor (Lektion 1)](teil1-kotlin-und-ktor/index.md), [Übung 1](uebungen/Uebung1.md)\
[Funktionen und Datentypen (Lektion 2)](teil2-funktionen-und-datentypen/index.md), [Übung 2](uebungen/Uebung2.md)

### Phase II: *Hübscher ist besser!*

Mit Hilfe der HTML-DSL (Doman Specific Language) wird die Anwendung "aufgehübscht". 
Dabei lernen Sie, wie DSLs in Kotlin funktionieren, 
wie man sie verwendet, 
und auch ganz einfach erweitern kann. 
Nebenher erfahren Sie einiges über das Typsystem von Kotlin.

[Klassen, Extensions und DSLs (Lektion 3)](teil3-klassen-extensions-und-dsls/index.md), [Übung 3](uebungen/Uebung3.md)

### Phase III: *Das Herz der Anwendung*

Jetzt wird die Anwendungslogik ausgebaut. 
Kotlin- und auch Java-Bibliotheken werden aufgerufen, 
eigene Klassen erstellt. 
Wir zeigen: Collections, Fallunterscheidungen, Klassen und Objekte, 
Konstruktoren und ein paar nützliche Tricks.

[Klassen, Maps (Lektion 4)](teil4-klassen-und-maps/index.md), [Übung 4](uebungen/Uebung4.md)

### Phase IV: *Und der ganze Rest der Welt …*

Wenn mehrere Anwender parallel bedient werden müssen 
und Austausch mit anderen Servern erforderlich wird, 
fängt es an, wirklich interessant zu werden. 
Wir zeigen, was Kotlin zu bieten hat: 
Asynchrone I/O, Koroutinen, Ktor-Session-Handling, Web-Sockets etc.

[REST Services und Data Classes (Lektion 5)](teil5-rest-services-und-data-classes/index.md), [Übung 5](uebungen/Uebung5.md)\
[Testen (Lektion 6)](teil6-testen/index.md)\
[Ausblick (Lektion 7)](teil7-ausblick/index.md)


### Fragen

## Zeiten
 
 | Wann                 | Was                                           |
 |----------------------|-----------------------------------------------|
 |  09:00 - 09:15       | **Intro**, Vorstellung                        |
 |  09:15 - 09:30       | **Installations-Check*                        |
 |   **Phase I**        | **Mehr als nur "Hallo Welt"**                 |
 |  09:30 - 10:00       | Einstieg in Kotlin und Ktor (Lektion 1)       |
 |  10:00 - 10:30       | Übung 1                                       |
 |    **Pause**         |                                               |
 |  10:45 - 11:30       | Funktionen und Datentypen (Lektion 2)         |
 |  11:30 - 12:30       | Übung 2                                       |
 |   **Mittag**         |                                               |
 |   **Phase II**       | **Hübscher ist besser!**                      |
 |  12:30 - 13:00       | Klassen, Extensions und DSLs (Lektion 3)      |
 |  13:00 - 13:45       | Übung 3                                       |
 |    **Pause**         |                                               |
 |  **Phase III**       | **Das Herz der Anwendung!**                   |
 |  14:00 - 14:30       | Klassen, Maps (Lektion 4)                     |
 |  14:30 - 14:45       | Übung 4                                       |
 |    **Pause**         |                                               |
 |  **Phase VI**        | **Und der ganze Rest der Welt ...**           |
 |  15:00 - 15:20       | REST Services und Data Classes (Lektion 5)    |
 |  15:20 - 16:00       | Übung 5                                       |
 |    **Pause**         |                                               |
 |  16:15 - 16:35       | KoTest und Mockk (Lektion 6)                  |
 |  16:35 - 17:00       | Ausblick und Fragen (Lektion 7)               |


## Kontakt

 | Name                 | Twitter       | Email                         |
 |----------------------|---------------|-------------------------------|
 | **Bjørn Stachmann**  | @old_stachi   | bjoern.stachmann@gmail.com    |


