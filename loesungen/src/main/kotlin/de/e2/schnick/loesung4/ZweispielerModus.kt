package de.e2.schnick.loesung5b

import de.e2.schnick.loesung5b.Ergebnis.SPIELER1_GEWINNT
import de.e2.schnick.loesung5b.Ergebnis.SPIELER2_GEWINNT
import de.e2.schnick.loesung5b.Wahl.PAPIER
import de.e2.schnick.loesung5b.Wahl.SCHERE
import de.e2.schnick.loesung5b.Wahl.STEIN
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.BODY
import kotlinx.html.ButtonType
import kotlinx.html.FormMethod
import kotlinx.html.body
import kotlinx.html.button
import kotlinx.html.form
import kotlinx.html.h1
import kotlinx.html.label
import kotlinx.html.p
import java.util.concurrent.ConcurrentHashMap

enum class Ergebnis {
    OFFEN,
    SPIELER1_GEWINNT,
    SPIELER2_GEWINNT,
    UNENTSCHIEDEN
}

enum class Wahl {
    STEIN, PAPIER, SCHERE
}

fun String.toWahl() = when (this.toLowerCase()) {
    "stein" -> STEIN
    "papier" -> PAPIER
    "schere" -> SCHERE
    else -> throw IllegalStateException("$this ist ungültige Wahl")
}


class Spiel(var wahl1: Wahl? = null, var wahl2: Wahl? = null) {
    fun ermittleErgebnis() = when {
        wahl1 == null || wahl2 == null -> Ergebnis.OFFEN
        wahl1 == wahl2 -> Ergebnis.UNENTSCHIEDEN
        wahl1 == PAPIER && wahl2 == STEIN -> SPIELER1_GEWINNT
        wahl1 == STEIN && wahl2 == SCHERE -> SPIELER1_GEWINNT
        wahl1 == SCHERE && wahl2 == PAPIER -> SPIELER1_GEWINNT
        else -> SPIELER2_GEWINNT
    }
}

fun Application.htmlModul() {

    val id2spiel = ConcurrentHashMap<String, Spiel>()

    routing {
        routing {
            get("/zweispielerstart/{spielid}/{spielerid}") {
                val spielid = call.parameters["spielid"] ?: throw IllegalArgumentException("Id fehlt")
                val spielerid = call.parameters["spielerid"] ?: throw IllegalArgumentException("spielerid fehlt")

                call.respondHtml {
                    body {
                        h1 {
                            text("Schnick Schnack Schnuck")
                        }
                        form(action = "/zweispieler/$spielid/$spielerid", method = FormMethod.get) {
                            label {
                                text("Deine Wahl:")
                            }
                            button(type = ButtonType.submit, name = "wahl") {
                                value = "papier"
                                text("Papier")
                            }
                            button(type = ButtonType.submit, name = "wahl") {
                                value = "stein"
                                text("Stein")
                            }
                        }
                    }
                }
            }

            get("/zweispieler/{spielid}/{spielerid}") {
                val spielid = call.parameters["spielid"] ?: throw IllegalArgumentException("Id fehlt")
                val spielerid = call.parameters["spielerid"] ?: throw IllegalArgumentException("spielerid fehlt")
                val wahl = call.parameters["wahl"]?.toWahl()

                if (wahl == null) {
                    call.respondText("Bitte Parameter 'wahl' angeben!")
                } else {
                    val spiel = id2spiel.getOrPut(spielid) { Spiel() }

                    when(spielerid) {
                        "1" -> spiel.wahl1 = wahl
                        "2" -> spiel.wahl2 = wahl
                        else -> throw IllegalArgumentException("spielerid falsch")
                    }
                    val ergebnis = spiel.ermittleErgebnis()

                    call.respondHtml {
                        body {
                            h1 {
                                text("Schnick Schnack Schnuck")
                            }

                            if (ergebnis == Ergebnis.OFFEN) {
                                paragraph("Warte auf anderen Spieler.")

                            } else {
                                paragraph("Spieler 1 hat gewählt ${spiel.wahl1}.")
                                paragraph("Spieler 2 hat gewählt ${spiel.wahl2}.")
                                paragraph("Das Ergebnis ist: $ergebnis.")
                            }
                        }
                    }
                }
            }
        }
    }
}

fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::htmlModul)

    server.start(wait = true)
}

fun BODY.paragraph(s: String) =
    p {
        text(s)
    }
