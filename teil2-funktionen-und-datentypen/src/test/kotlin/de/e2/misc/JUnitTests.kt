package de.e2.misc

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class JUnitTests {

    @Test
    fun manKannGanzNormaleJUnitTestsSchreiben() {

        val result = 2 * 21

        assertEquals(42, result)
        assertThrows<ArithmeticException> { result / 0 }
    }

    @Test
    fun `mit Backticks darf man auch Leer- und (manche) Sonderzeichen in Namen nutzen`() {

        assertEquals(4711, 4700 + 11)
    }

}


