package de.e2.adder

import com.fasterxml.jackson.databind.JsonMappingException
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

data class AddRequest(val arg1: Int, val arg2: Int)
data class AddResult(val sum: Int)

fun Application.adderApi() {
    //<editor-fold desc="Json - Details" defaultstate="collapsed">
    install(ContentNegotiation) {
        jackson()
    }

    install(StatusPages) {
        exception<JsonMappingException> {
            call.respond(HttpStatusCode.UnprocessableEntity)
        }
    }
    //</editor-fold>

    routing {
        post("/add") {
            val req: AddRequest = call.receive()

            println("Request $req")

            call.respond(AddResult(req.arg1 + req.arg2))
        }
    }
}

fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::adderApi)
    server.start(wait = true)
}