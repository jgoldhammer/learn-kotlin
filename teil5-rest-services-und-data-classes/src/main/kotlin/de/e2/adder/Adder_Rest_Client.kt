package de.e2.adder

import io.ktor.client.HttpClient
import io.ktor.client.features.DefaultRequest
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.http.ContentType
import io.ktor.http.contentType
import kotlinx.coroutines.runBlocking

fun main(): Unit = runBlocking {
    val client = HttpClient {
        install(JsonFeature) {
            serializer = JacksonSerializer()
        }

        install(DefaultRequest) {
            contentType(ContentType.Application.Json)
            url.host = "localhost"
            url.port = 8080
        }
    }


    val addRequest1 = AddRequest(1, 2)
    println("Request1: $addRequest1")
    val addResult1 = client.post<AddResult> {
        url("http://localhost:8080/add")
        contentType(ContentType.Application.Json)
        body = addRequest1
    }
    println("Ergebnis1: ${addResult1.sum}")

    val addRequest2 = addRequest1.copy(arg1 = 10)
    println("Request2: $addRequest2")
    val (sum) = client.post<AddResult>(path = "add", body = addRequest2)
    println("Ergebnis2: $sum")
}