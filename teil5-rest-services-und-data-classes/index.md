# Rest, Data-Klassen, Koroutinen

* [Data-Klassen (bst)](src/test/kotlin/de/e2/misc/DataClasses.kt)
* [Rest-Server und Data-Klassen (bst)](src/main/kotlin/de/e2/adder/Adder_Rest_Api.kt)
* [Rest-Client und Destructuring (bst)](src/main/kotlin/de/e2/adder/Adder_Rest_Client.kt) 
* [Uebung 5](../uebungen/Uebung5.md)
