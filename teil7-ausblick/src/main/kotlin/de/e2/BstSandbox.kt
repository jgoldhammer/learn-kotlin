package de.e2

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.intellij.markdown.flavours.commonmark.CommonMarkFlavourDescriptor
import org.intellij.markdown.html.HtmlGenerator
import org.intellij.markdown.parser.MarkdownParser

data class HalloWelt(
        val hallo: String,
        val welt: String,
        val egal: String
)

fun main() {


    val mapper = ObjectMapper().registerModule(KotlinModule())
    println(mapper.writeValueAsString(HalloWelt("hello", "world", "not important")))

    val src = "Some *Markdown*"
    val flavour = CommonMarkFlavourDescriptor()
    val parsedTree = MarkdownParser(flavour).buildMarkdownTreeFromString(src)
    println(HtmlGenerator(src, parsedTree, flavour).generateHtml())


}