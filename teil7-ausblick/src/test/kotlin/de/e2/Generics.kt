package de.e2

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.databind.ObjectMapper
import io.kotest.core.spec.style.StringSpec
import java.io.File

open class DataCompanion<C> {

    val mapper = ObjectMapper(JsonFactory())

    fun write(c: C, file: File) {
        file.parentFile.mkdirs()
        mapper.writeValue(file, c)
    }

    inline fun <reified D : C> read(file: File) = mapper.readValue(file, D::class.java)
}

data class Name(var vorname: String = "", var nachname: String = "") {
    companion object : DataCompanion<Name>()
}

class Generics : StringSpec({

    "Serializing and deserializing" {
        val name = Name("Hallo", "Welt")
        val file = File("build/tmp/hw.json")
        Name.write(name, file)

        val name2: Name = Name.read(file)
        println(name2)
    }
})