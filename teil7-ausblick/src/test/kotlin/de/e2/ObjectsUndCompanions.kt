package de.e2

import io.ktor.server.netty.Netty
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.util.ArrayList

object Notizblock : ArrayList<String>()

class ObjectsUndCompanions {

    @Test
    fun `objects sind von Kotlin verwaltete Singletons`() {
        assertTrue(Notizblock.isEmpty())

        Notizblock.add("Einkaufen gehen")
        Notizblock.add("Kuchen backen")

        assertEquals(2, Notizblock.size)

        // Beispiel: Netty in Ktor. Tipp: Ctrl-Click auf `Netty`
        assertNotNull(Netty)
    }

    data class Kunde(val nr: Int, val name: String) {

        companion object {
            var nextNr: Int = 100

            fun register(name: String) =
                Kunde(nextNr++, name)
        }
    }

    @Test
    fun `companion objects sind die Ersatzdroge für java static`() {
        val k1 = Kunde.register("Gosling")
        val k2 = Kunde.register("Jemerow")

        assertEquals(100, k1.nr)
        assertEquals(101, k2.nr)
    }

}