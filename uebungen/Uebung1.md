# Beschreibung der Übung

Erzeuge eine Ktor-Anwendung die auf die URL http://localhost:8080/spiel
reagiert und als Ergebnis den String
[`Schnick Schnack Schnuck`](https://de.wikipedia.org/wiki/Schere,_Stein,_Papier)
zurückgibt.

## Zusatzaufgaben
* Wiederhole die Ausgabe mehrfach mit der Funktion: `repeat`
    * ACHTUNG: `respondText()` kann nur einmal aufgerufen werden. Nutze `respondTextWriter`
* Wiederhole die Ausgabe mehrfach mit eine For-Schleife.

# Kenntnisse
* `fun main()` - als Main-Funktion
* `val` für unveränderliche Variablen
* Named-Parameter `port=8080`
* Trailing Lambdas `repeat(5) { println(it) }`
* `call.respondText("Hallo")` für Ausgaben an den Client
* For-Schleife: `for ( i in 1..10 )`

# Code-Beispiele
 * `teil1-kotlin-und-ktor/src/main/kotlin/de/e2/helloworld/HelloWorld_1.kt`
 * `teil1-kotlin-und-ktor/src/main/kotlin/de/e2/helloworld/HelloWorld_2a_Repeat.kt`