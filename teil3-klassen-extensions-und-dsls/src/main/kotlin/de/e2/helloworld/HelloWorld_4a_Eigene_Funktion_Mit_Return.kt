package de.e2.ktor.p01_helloworld

import io.ktor.application.call
import io.ktor.http.Parameters
import io.ktor.response.respondTextWriter
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

fun requiredParameter(parameters: Parameters, name: String): String {
    val parameter = parameters[name]
    if (parameter == null) {
        throw IllegalStateException("Parameter $name is required")
    }

    return parameter
}

fun main() {
    val server = embeddedServer(Netty, port = 8080) {
        routing {
            get("/hello/{count}") {
                val countAsInt: Int = requiredParameter(call.parameters, "count").toInt()

                call.respondTextWriter {
                    repeat(countAsInt) {
                        write("World $it\n")
                    }
                }
            }
        }
    }
    server.start(wait = true)
}