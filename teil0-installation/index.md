# Installationsanleitung für den Kotlin-Workshop

Im Kotlin-Workshop werden Sie auf ihrem eigenen Laptop 
eine kleine Anwendung selber programmieren.

Dafür benötigen sie natürlich eine Kotlin-Entwicklungsumgebung.

Da im Konferenz-WLAN die Bandbreite begrenzt ist,
empfehle ich dringend (!), 
dass Sie die Entwicklungsumgebung vor dem Workshop installieren.
Hierzu dient diese Anleitung.

## Überblick

Der Workshop kann auf *Linux*, *Windows* oder *macOS* durchgeführt werden.
Es wird folgendes benötigt:

 1. **Intellij IDEA** als Entwicklungsumgebung
 2. **Java**, genauer gesagt ein SDK
 3. **Kotlin-Plugin für IDEA** in der aktuellen Version
 4. **Git** weil das Workshop-Projekt auf Gitlab abgelegt ist.
 5. **Beispiele und Übungen** für diesen Workshop(https://gitlab.com/kapitel26/learn-kotlin)
 6. **Libraries**, die zum Build benötigt werden.

Natürlich können Sie einzelne Schritte überspringen, 
wenn die entsprechenden Komponenten bei Ihnen schon vorhanden sind.

Nur **die letzten beiden Schritte** "Beispiele und Übungen" und "Libraries",
sollten sie **unbedingt ausführen**, 
damit die für den Build-Prozess benötigten Libraries zum Workshop geladen sind.

## IntelliJ IDEA

 * **Linux (Ubuntu)**
    - `sudo snap install intellij-idea-community --classic`
 * **Windows**
    - [Installer downloaden](https://www.jetbrains.com/idea/download/#section=windows) und ausführen.
 * **macOs**
    - [Installer downloaden](https://www.jetbrains.com/idea/download/#section=mac) und ausführen.

Beim Auswahldialogen im Installationsprozess können 
Sie einfach die vorausgewählten Optionen nehmen.

## Java SDK auswählen.

Vermutlich haben Sie, als Besucher der Jax, bereits ein (oder mehrere) SDK installiert 
(falls nicht, finden sie beispielsweise [hier](https://jdk.java.net/14/) eines).

Nach dem Start von IntelliJ IDEA erscheint der Projektauswahldialog.
(Falls nicht, sind evtl. schon Projekte offen. 
Schließen sie diese mit "File | Close Project" bis der Dialog erscheint).

![Project Selection](idea-project-selection-dialog.png)

Rechts unten finden sich die Einstellungen.

![Configure](configure.png)

Wählen Sie dort `Project Structure for new Projects`,

![Project Structure](project-structure-for-new-projects.png)

In der Auswahlbox für das `Project SDK`, 
können Sie von IntelliJ bereits erkannt SKDs auswählen
(oder `Add JDK ...` oder `Download JDK` eine hinzufügen),
z.B. `Oracle OpenJDK 14.0.2`.

### Kotlin testen

Es sollte wieder Projektauswahldialog sichtbar sein.
(Falls nicht, sind evtl. schon Projekte offen. 
Schließen sie diese mit `File | Close Project bis der Dialog erscheint).

![Project Selection](idea-project-selection-dialog.png)

Wir können jetzt ein Kotlin-Projekt mit `New Project ...` anlegen, etwas so:

![Project Selection](select-kotlin-project.png)

Hier wählen wir Kotlins


![Create Project](create-hello-project.png)

In dem neuen Projekt legen wir eine Datei an:

![New File](create-kotlin-file.png)

Und schreiben kein Folgendes:

![New File](kotlin-hello-world.png)

Jetzt den grüngen Pfeil klicken und ihr erstes Kotlin-Programm sollte starten.

## Kotlin Plugin aktualisieren

Am besten ist es, wenn Sie die aktuelle Version des Kotlin-Plugins nutzen.
Voraussichtlich wird IntelliJ IDEA von alleine vorschlagen,
die neueste Version des Kotlin-Plugins zu installieren:

![New Kotling Plugin Version](neue-kotlin-plugin-version.png)

Falls nicht, können sie unter `File | Settings ...` im Abschnitt `Plugins`,
das Kotlin-Plugin aktualisieren (`1.4.0-release-IJ2020.2-1`)

## Git Installieren

Installieren Sie Git (https://git-scm.com/downloads, bzw. auf Unbunte: `sudo apt-get install git`)

## Beispiele und Übungen vorbereiten

Jetzt können sie die Beispiele und Übungen über das Menü `VCS | Get from Version Control` laden. 
Die URL ist:

    https://gitlab.com/kapitel26/learn-kotlin.git

![New Kotling Plugin Version](clone-workshop-samples.png)

Dann klicken wir den Gradle-Build-Elefanten an:

![Gradle](gradle-area.png)

Und starten den Build:

    gradle clean build

![gradle clean build](gradle-clean-build.png)

Wenn der Build erfolgreich durchgelaufen ist,
wurden alle Abhängigkeiten geladen,
und Sie sind bereit für den Workshop!






