package de.e2.testen

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkConstructor
import io.mockk.slot
import io.mockk.spyk
import io.mockk.verify
import java.time.Month
import kotlin.random.Random

class Mocking : StringSpec({

    class Address(val city: String)

    class Person(val name: String, val address: Address)

    "simple mock" {
        val mockPerson = mockk<Person>()
        every { mockPerson.name } returns "Joe"

        mockPerson.name shouldBe "Joe"
    }

    "call chain" {
        val mockPerson = mockk<Person>()
        every { mockPerson.address.city } returns "Hamburg"

        mockPerson.address.city shouldBe "Hamburg"
    }

    "mock verify confirm" {
        val mockRandom = mockk<Random>()
        every { mockRandom.nextInt(any()) } returns 0

        val months = Month.values()
        val randomMonth = months.random(mockRandom)
        randomMonth shouldBe Month.JANUARY

        verify(atLeast = 1) {
            mockRandom.nextInt(any())
        }

        confirmVerified(mockRandom)
    }

    "constructor mock" {
        mockkConstructor(Person::class)

        every { anyConstructed<Person>().name } returns "Joe"

        Person("Jan", Address("Hamburg")).name shouldBe "Joe"
    }

    "spy" {
        val person = Person("Jan", Address("Hamburg"))
        val spyPerson = spyk(person)
        spyPerson.address

        verify {
            spyPerson.address
        }

        confirmVerified(spyPerson)
    }

    class Calculator {
        fun add(a: Int, b: Int) = a + b
    }

    "capture with mock" {
        val calcMock = mockk<Calculator>()

        val aSlot = slot<Int>()
        val bSlot = mutableListOf<Int>()

        every { calcMock.add(capture(aSlot), capture(bSlot)) } returns 0

        calcMock.add(1, 2)
        calcMock.add(10, 20)

        aSlot.captured shouldBe 10
        bSlot shouldBe listOf(2, 20)
    }

    "capture with spy" {
        val calcSpy = spyk<Calculator>()

        val aSlot = slot<Int>()
        val bSlot = mutableListOf<Int>()

        every { calcSpy.add(capture(aSlot), capture(bSlot)) } answers {
            callOriginal()
        }
        calcSpy.add(1, 2)
        calcSpy.add(10, 20)

        aSlot.captured shouldBe 10
        bSlot shouldBe listOf(2, 20)
    }
})